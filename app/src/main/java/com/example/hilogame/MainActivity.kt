package com.example.hilogame

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    var guess = Random.nextInt(1, 1000)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onSubmitClick(view: View)
    {
        if (Guess.text.toString().toInt() == guess)
        {
            Toast.makeText(this, "You Got It!", Toast.LENGTH_SHORT).show()
            help.text = "Correct!"
        }
        else if (Guess.text.toString().toInt() < guess)
        {
            help.text = "Higher"
        }
        else
        {
            help.text = "Lower"
        }
    }

    fun onNewNumberClick(view: View)
    {
        guess = Random.nextInt(0, 1000)
    }
}
